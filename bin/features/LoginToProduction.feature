# We can add two tags also.
@bvt @prod
Feature: Login to Productction
  In order to peform successful login to prodution 
  As a user
  I want to enter valid username and password

  Background: 
    Given user navigate to facebook website
    When user validates home page title

  Scenario: In order to verify login to production facebook
    Then user entered "valid" username
    And user entered "valid" password
    And use validates captcha image
    Then user "shouldbe" logged in successfully

  Scenario: In order to verify login to production facebook
    Then user entered "invalid" username
    And user entered "invalid" password
    And use validates captcha image
    Then user "shouldnot" logged in successfully

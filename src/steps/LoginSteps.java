package steps;

import java.util.List;
import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {

	// Before will execute before every scenario

	// After will execute after every scenarion

	// HOOKS will run only if there is an appropriate declaration done.

	// @before("@prod") will run before every prod methods
	// @after("@prod") will run after every prod methods
	
	
	
	// Video - 18 TESTNG with cucumber 
	
	/*
	 * To use TESTNG with cuucmber 
	 *  	Add TESTNG library from mvn repo
	 *  	Extends steps class wihh AbstractTestNGCucumber interface
	 *  	Add TESTNG plugin so that you can create testng.xml file
	 *  	Add the ruuner classs in testNG.xml 
	 *  	Seelect Library of TESTNG insted of Junit
	 *  	run testng.xml as testNG
	 */

	@Before("@prod")
	public void setup() {
		System.out.println("open the browser");
	}

	@After("@prod")
	public void tearDown() {
		System.out.println("close the browser");
	}

	@Given("^user navigate to facebook website$")
	public void user_navigate_to_facebook_website() throws Throwable {
		System.out.println("@given method text");
	}

	@When("^user validates home page title$")
	public void user_validates_home_page_title() throws Throwable {

		System.out.println("@when method text");
	}

	@Then("^user entered \"([^\"]*)\" username$")
	public void user_entered_valid_username(String username) throws Throwable {
		System.out.println("@then method text u entered " + username + " username");
	}

	@And("^user entered \"([^\"]*)\" password$")
	public void password_entered_valid_password(String password) throws Throwable {
		System.out.println("@and method text u entered  " + password + " password");
	}

	@And("^user select the age category$")
	public void user_select_the_age_category(DataTable table) throws Throwable {

		List<Map<String, String>> data = table.asMaps(String.class, String.class);

		System.out.println("@And use select age category : " + data.get(0).get("Age")
				+ ("--Selected Location as: " + data.get(1).get("location")));
	}

	@Then("^user \"([^\"]*)\" logged in successfully$")
	public void user_logged_in_successfully(String validateLogin) throws Throwable {
		System.out.println("@last then method text use" + validateLogin + " succesfull logged in");
	}

}

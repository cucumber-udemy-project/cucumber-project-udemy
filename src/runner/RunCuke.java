package runner;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

//Runner class will help yo to run feature file
// when you are not using cucumber plugin
@RunWith(Cucumber.class)

// To call featues folder , or steps folder

//tags is used to call the tag which u want to execute 
@CucumberOptions(features= {"src/features"},glue= {"steps"},monochrome=true, tags= {"@sanity,@bvt"})

public class RunCuke {
	

}
 
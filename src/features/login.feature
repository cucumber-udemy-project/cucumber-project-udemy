@sanity
Feature: Login
  In order to peform successful login 
  As a user
  I want to enter valid username and password

  Scenario Outline: In order to verify login to facebook
    Given user navigate to facebook website
    When user validates home page title
    Then user entered "<username>" username
    And user entered "<password>" password
    And user select the age category
      | Age      | location |
      | Below 18 | India    |
      | Above 18 | US       |
    Then user "<loginType>" logged in successfully

    Examples: 
      | username | password | loginType |
      | valid    | valid    | shouldbe  |
      | invalid  | invalid  | shouldnot |
